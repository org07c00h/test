import { Player } from "../objects/Player";
import { Star } from "../objects/Star";

export class GameScene extends Phaser.Scene {
    private _player!: Player;
    private _star!: Star;
    private _counter: number;
    private _counterText!: Phaser.GameObjects.Text;

    constructor() {
        super({ key: "GameScene" });

        this._counter = 0;
    }

    public preload(): void {
        this.load.image("player", "../assets/player.png");
        this.load.image("star", "../assets/coin.png");
    }

    public create(): void {
        this._player = new Player(this, 32, 32);
        this._star = new Star(this, 100, 100);

        const style: object = {
            fontFamily: "Roboto",
            fontSize: 28,
            fill: "#ffffff",
        };
        this._counterText = this.add.text(10, this.sys.canvas.height - 50,
            this._counter.toString(), style);
    }

    public update(): void {
        this._player.update();
        this._star.update();

        const intersection: boolean = Phaser.Geom.Intersects.RectangleToRectangle(
            this._player.getBounds(), this._star.getBounds());
        if (intersection) {
            this.updateCoinStatus();
        }
    }

    private updateCoinStatus(): void {
        this._counter++;
        this._star.changePosition();
        this._counterText.setText(this._counter.toString());
    }
}
