export class Star extends Phaser.GameObjects.Image {

    constructor(scene: Phaser.Scene, x: number, y: number) {
        super(scene, x, y, "star");

        this.setOrigin(0.5, 0.5);
        this.setScale(3, 3);
        this.scene.add.existing(this);
    }

    public changePosition(): void {
        this.x = Phaser.Math.RND.integerInRange(100, 500);
        this.y = Phaser.Math.RND.integerInRange(100, 500);
    }
}
