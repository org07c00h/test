export class Player extends Phaser.GameObjects.Image {
    private _cursor: Phaser.Input.Keyboard.CursorKeys;
    private _direction: Phaser.Math.Vector2;

    constructor(scene: Phaser.Scene, x: number, y: number) {
        super(scene, x, y, "player");

        this.setOrigin(0.5, 0.5);
        this.setScale(2, 2);
        this.scene.add.existing(this);
        this._cursor = this.scene.input.keyboard.createCursorKeys();
        this._direction = new Phaser.Math.Vector2();
    }
    public update(): void {
        this.handleInput();
    }

    public handleInput(): void {
        if (this._cursor.right!.isDown) {
            this._direction.x = 1;
        } else if (this._cursor.left!.isDown) {
            this._direction.x = -1;
        } else {
            this._direction.x = 0;
        }

        if (this._cursor.up!.isDown) {
            this._direction.y = -1;
        } else if (this._cursor.down!.isDown) {
            this._direction.y = 1;
        } else {
            this._direction.y = 0;
        }

        if (this._direction.x !== 0 || this._direction.y !== 0) {
            this._direction.normalize();
            this.x += this._direction.x * 5;
            this.y += this._direction.y * 5;
        }
    }
}
