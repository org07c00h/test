import { GameScene } from "./scenes/GameScene";

class Game extends Phaser.Game {
    constructor(config: GameConfig) {
        super(config);
    }
}

const gameConfig: GameConfig = {
    backgroundColor: "#3A99D9",
    scene: [ GameScene ],
    title: "Test Project",
    type: Phaser.WEBGL,
    width: 800,
    height: 600,
    render: {
        pixelArt: true,
    },
    input: {
        keyboard: true,
    },
};

window.onload = () => {
    const game = new Game(gameConfig);
};
